import { ReviewEntity } from './entities/review.entity';
import { CreateReviewDto } from './dto/create-review.dto';
import { FindOneOptions, Repository } from 'typeorm';
export declare class ReviewService {
    private reviewsRepository;
    constructor(reviewsRepository: Repository<ReviewEntity>);
    create(userId: string, createReviewDto: CreateReviewDto): Promise<any>;
    findAll(): Promise<ReviewEntity[]>;
    findOneOrFail(options: FindOneOptions<ReviewEntity>): Promise<ReviewEntity>;
    remove(id: string): Promise<void>;
}
