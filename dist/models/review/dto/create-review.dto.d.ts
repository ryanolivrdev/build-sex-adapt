export declare class CreateReviewDto {
    grade: number;
    comment: string;
    establishmentId: string;
}
