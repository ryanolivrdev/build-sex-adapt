"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateReviewDto = void 0;
const openapi = require("@nestjs/swagger");
const index_messages_1 = require("../../../common/helpers/exceptions/messages/index.messages");
const class_validator_1 = require("class-validator");
class CreateReviewDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { grade: { required: true, type: () => Number }, comment: { required: true, type: () => String }, establishmentId: { required: true, type: () => String } };
    }
}
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: index_messages_1.HttpCustomMessages.VALIDATION.REVIEW.GRADE.REQUIRED }),
    (0, class_validator_1.IsNumber)({ maxDecimalPlaces: 1 }, { message: index_messages_1.HttpCustomMessages.VALIDATION.REVIEW.GRADE.INVALID }),
    (0, class_validator_1.Min)(0, { message: index_messages_1.HttpCustomMessages.VALIDATION.REVIEW.GRADE.REQUIRED }),
    (0, class_validator_1.Max)(5, { message: index_messages_1.HttpCustomMessages.VALIDATION.REVIEW.GRADE.REQUIRED }),
    __metadata("design:type", Number)
], CreateReviewDto.prototype, "grade", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({
        message: index_messages_1.HttpCustomMessages.VALIDATION.REVIEW.COMMENT.REQUIRED
    }),
    (0, class_validator_1.IsString)({ message: index_messages_1.HttpCustomMessages.VALIDATION.REVIEW.COMMENT.INVALID }),
    (0, class_validator_1.MaxLength)(125, {
        message: index_messages_1.HttpCustomMessages.VALIDATION.REVIEW.COMMENT.LENGTH
    }),
    __metadata("design:type", String)
], CreateReviewDto.prototype, "comment", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateReviewDto.prototype, "establishmentId", void 0);
exports.CreateReviewDto = CreateReviewDto;
//# sourceMappingURL=create-review.dto.js.map