import { UserEntity } from '@user/entities/user.entity';
import { IReviewEntity } from '../interfaces/review.interface';
import { BaseEntity } from '@base/entities/base-entity.entity';
import { EstablishmentEntity } from '../../establishment/entities/establishment.entity';
export declare class ReviewEntity extends BaseEntity implements IReviewEntity {
    user: UserEntity;
    grade: number;
    comment: string;
    establishment: EstablishmentEntity;
}
