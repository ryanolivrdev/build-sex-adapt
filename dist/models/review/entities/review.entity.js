"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewEntity = void 0;
const openapi = require("@nestjs/swagger");
const user_entity_1 = require("../../user/entities/user.entity");
const base_entity_entity_1 = require("../../base/entities/base-entity.entity");
const typeorm_1 = require("typeorm");
const establishment_entity_1 = require("../../establishment/entities/establishment.entity");
let ReviewEntity = class ReviewEntity extends base_entity_entity_1.BaseEntity {
    static _OPENAPI_METADATA_FACTORY() {
        return { user: { required: true, type: () => require("../../user/entities/user.entity").UserEntity }, grade: { required: true, type: () => Number }, comment: { required: true, type: () => String }, establishment: { required: true, type: () => require("../../establishment/entities/establishment.entity").EstablishmentEntity } };
    }
};
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.UserEntity, (user) => user.reviews, { onDelete: 'CASCADE' }),
    __metadata("design:type", user_entity_1.UserEntity)
], ReviewEntity.prototype, "user", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], ReviewEntity.prototype, "grade", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], ReviewEntity.prototype, "comment", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => establishment_entity_1.EstablishmentEntity, (establishment) => establishment.reviews, { onDelete: 'CASCADE' }),
    __metadata("design:type", establishment_entity_1.EstablishmentEntity)
], ReviewEntity.prototype, "establishment", void 0);
ReviewEntity = __decorate([
    (0, typeorm_1.Entity)({ name: 'review' })
], ReviewEntity);
exports.ReviewEntity = ReviewEntity;
//# sourceMappingURL=review.entity.js.map