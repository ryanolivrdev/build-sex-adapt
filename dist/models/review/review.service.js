"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReviewService = void 0;
const review_entity_1 = require("./entities/review.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
let ReviewService = class ReviewService {
    constructor(reviewsRepository) {
        this.reviewsRepository = reviewsRepository;
    }
    async create(userId, createReviewDto) {
        try {
            const review = this.reviewsRepository.create(Object.assign({ user: userId }, createReviewDto));
            await this.reviewsRepository.save(review);
            return review;
        }
        catch (error) {
            return error;
        }
    }
    async findAll() {
        return await this.reviewsRepository.find({
            relations: {
                user: true
            },
            select: {
                user: {
                    id: true,
                    email: true,
                    name: true
                }
            }
        });
    }
    async findOneOrFail(options) {
        try {
            return await this.reviewsRepository.findOneOrFail(Object.assign(Object.assign({}, options), { relations: {
                    user: true
                }, select: {
                    user: {
                        id: true,
                        email: true,
                        name: true
                    }
                } }));
        }
        catch (error) {
            throw new common_1.NotFoundException(error.message);
        }
    }
    async remove(id) {
        await this.findOneOrFail({
            where: { id }
        });
        await this.reviewsRepository.delete({ id });
        return;
    }
};
ReviewService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(review_entity_1.ReviewEntity)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], ReviewService);
exports.ReviewService = ReviewService;
//# sourceMappingURL=review.service.js.map