import { ReviewService } from './review.service';
import { CreateReviewDto } from './dto/create-review.dto';
export declare class ReviewController {
    private readonly reviewService;
    constructor(reviewService: ReviewService);
    create(userId: string, createReviewDto: CreateReviewDto): Promise<any>;
    findAll(): Promise<import("./entities/review.entity").ReviewEntity[]>;
    findOne(id: string): Promise<import("./entities/review.entity").ReviewEntity>;
    remove(id: string): Promise<void>;
}
