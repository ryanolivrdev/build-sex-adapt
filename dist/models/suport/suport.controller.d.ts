import { CreateSuportDto } from './dto/create-suport.dto';
import { SuportService } from './suport.service';
export declare class SuportController {
    private readonly suportService;
    constructor(suportService: SuportService);
    create(userId: string, createSuportDto: CreateSuportDto): Promise<any>;
    findOne(id: string): Promise<import("./entities/suport.entity").SuportEntity>;
    remove(id: string): Promise<void>;
}
