"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuportService = void 0;
const mail_service_1 = require("./../../services/mail/mail.service");
const suport_entity_1 = require("./entities/suport.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
let SuportService = class SuportService {
    constructor(suportRepository, mailService) {
        this.suportRepository = suportRepository;
        this.mailService = mailService;
    }
    async create(userId, createSuportDto) {
        try {
            const suport = this.suportRepository.create(Object.assign({ user: userId }, createSuportDto));
            await this.suportRepository.save(suport);
            const supportFinder = await this.suportRepository.findOne({
                where: {
                    id: userId
                },
                relations: ['user'],
                select: {
                    user: {
                        email: true
                    }
                }
            });
            await this.mailService.sendSuportMail({
                subject: createSuportDto.subject,
                title: createSuportDto.title,
                subtitle: createSuportDto.subtitle,
                content: createSuportDto.message
            }, supportFinder.user.email);
            return suport;
        }
        catch (error) {
            return error;
        }
    }
    async findAll() {
        return await this.suportRepository.find({
            relations: {
                user: true
            },
            select: {
                user: {
                    id: true,
                    email: true,
                    name: true
                }
            }
        });
    }
    async findOneOrFail(options) {
        try {
            return await this.suportRepository.findOneOrFail(Object.assign(Object.assign({}, options), { relations: {
                    user: true
                }, select: {
                    user: {
                        id: true,
                        email: true,
                        name: true
                    }
                } }));
        }
        catch (error) {
            throw new common_1.NotFoundException(error.message);
        }
    }
    async remove(id) {
        await this.findOneOrFail({
            where: { id }
        });
        await this.suportRepository.delete({ id });
        return;
    }
};
SuportService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(suport_entity_1.SuportEntity)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        mail_service_1.MailService])
], SuportService);
exports.SuportService = SuportService;
//# sourceMappingURL=suport.service.js.map