import { MailService } from './../../services/mail/mail.service';
import { CreateSuportDto } from './dto/create-suport.dto';
import { SuportEntity } from '@models/suport/entities/suport.entity';
import { FindOneOptions, Repository } from 'typeorm';
export declare class SuportService {
    private suportRepository;
    private readonly mailService;
    constructor(suportRepository: Repository<SuportEntity>, mailService: MailService);
    create(userId: string, createSuportDto: CreateSuportDto): Promise<any>;
    findAll(): Promise<SuportEntity[]>;
    findOneOrFail(options: FindOneOptions<SuportEntity>): Promise<SuportEntity>;
    remove(id: string): Promise<void>;
}
