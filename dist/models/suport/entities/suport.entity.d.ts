import { UserEntity } from '@models/user/entities/user.entity';
import { ISuportEntity } from '../interfaces/suport.interface';
import { BaseEntity } from './../../base/entities/base-entity.entity';
export declare class SuportEntity extends BaseEntity implements ISuportEntity {
    user: UserEntity;
    message: string;
}
