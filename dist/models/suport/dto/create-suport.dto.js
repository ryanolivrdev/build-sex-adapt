"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateSuportDto = void 0;
const openapi = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class CreateSuportDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { message: { required: true, type: () => String }, subject: { required: true, type: () => String }, title: { required: true, type: () => String }, subtitle: { required: true, type: () => String } };
    }
}
__decorate([
    (0, class_validator_1.IsString)({ message: 'A mensagem deve ser um texto.' }),
    (0, class_validator_1.IsNotEmpty)({ message: 'A mensagem é obrigatória.' }),
    __metadata("design:type", String)
], CreateSuportDto.prototype, "message", void 0);
__decorate([
    (0, class_validator_1.IsString)({ message: 'O assunto deve ser um texto.' }),
    (0, class_validator_1.IsNotEmpty)({ message: 'O assunto é obrigatório.' }),
    __metadata("design:type", String)
], CreateSuportDto.prototype, "subject", void 0);
__decorate([
    (0, class_validator_1.IsString)({ message: 'O titulo deve ser um texto.' }),
    (0, class_validator_1.IsNotEmpty)({ message: 'O titulo é obrigatório.' }),
    __metadata("design:type", String)
], CreateSuportDto.prototype, "title", void 0);
__decorate([
    (0, class_validator_1.IsString)({ message: 'O subtitulo deve ser um texto.' }),
    (0, class_validator_1.IsNotEmpty)({ message: 'O subtitulo é obrigatório.' }),
    __metadata("design:type", String)
], CreateSuportDto.prototype, "subtitle", void 0);
exports.CreateSuportDto = CreateSuportDto;
//# sourceMappingURL=create-suport.dto.js.map