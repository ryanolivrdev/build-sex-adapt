export declare class CreateSuportDto {
    message: string;
    subject: string;
    title: string;
    subtitle: string;
}
