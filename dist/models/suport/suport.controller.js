"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuportController = void 0;
const openapi = require("@nestjs/swagger");
const create_suport_dto_1 = require("./dto/create-suport.dto");
const suport_service_1 = require("./suport.service");
const common_1 = require("@nestjs/common");
const decorators_1 = require("../../common/decorators");
const swagger_1 = require("@nestjs/swagger");
let SuportController = class SuportController {
    constructor(suportService) {
        this.suportService = suportService;
    }
    async create(userId, createSuportDto) {
        return await this.suportService.create(userId, createSuportDto);
    }
    findOne(id) {
        return this.suportService.findOneOrFail({ where: { id } });
    }
    remove(id) {
        return this.suportService.remove(id);
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, common_1.HttpCode)(common_1.HttpStatus.CREATED),
    openapi.ApiResponse({ status: common_1.HttpStatus.CREATED, type: Object }),
    __param(0, (0, decorators_1.GetCurrentUserId)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, create_suport_dto_1.CreateSuportDto]),
    __metadata("design:returntype", Promise)
], SuportController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(':id'),
    openapi.ApiResponse({ status: 200, type: require("./entities/suport.entity").SuportEntity }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], SuportController.prototype, "findOne", null);
__decorate([
    (0, common_1.Delete)(':id'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], SuportController.prototype, "remove", null);
SuportController = __decorate([
    (0, swagger_1.ApiTags)('Support Routes'),
    (0, common_1.Controller)('suport'),
    __metadata("design:paramtypes", [suport_service_1.SuportService])
], SuportController);
exports.SuportController = SuportController;
//# sourceMappingURL=suport.controller.js.map