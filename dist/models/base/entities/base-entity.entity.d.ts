import { IBaseEntity } from '../interfaces/base-entity.interface';
export declare class BaseEntity implements IBaseEntity {
    id: string;
    createdAt: Date;
    updatedAt: Date;
}
