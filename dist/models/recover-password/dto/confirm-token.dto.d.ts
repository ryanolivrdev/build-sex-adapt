export declare class ConfirmTokenDto {
    email: string;
    token: number;
}
