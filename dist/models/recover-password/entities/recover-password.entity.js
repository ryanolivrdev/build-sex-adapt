"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecoverPasswordEntity = void 0;
const openapi = require("@nestjs/swagger");
const generateRandomDigits_util_1 = require("./../../../utils/generateRandomDigits.util");
const typeorm_1 = require("typeorm");
let RecoverPasswordEntity = class RecoverPasswordEntity {
    defaultValues() {
        this.token = (0, generateRandomDigits_util_1.generateRandomDigits)(0, 9999999);
    }
    static _OPENAPI_METADATA_FACTORY() {
        return { token: { required: true, type: () => Number }, email: { required: true, type: () => String }, status: { required: true, type: () => Object }, createdAt: { required: true, type: () => Date }, updatedAt: { required: true, type: () => Date } };
    }
};
__decorate([
    (0, typeorm_1.Column)({ nullable: true, update: false }),
    __metadata("design:type", Number)
], RecoverPasswordEntity.prototype, "token", void 0);
__decorate([
    (0, typeorm_1.PrimaryColumn)({ update: false }),
    __metadata("design:type", String)
], RecoverPasswordEntity.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: 'PENDING', insert: false, nullable: true }),
    __metadata("design:type", String)
], RecoverPasswordEntity.prototype, "status", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)({ name: 'created_at' }),
    __metadata("design:type", Date)
], RecoverPasswordEntity.prototype, "createdAt", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ name: 'updated_at' }),
    __metadata("design:type", Date)
], RecoverPasswordEntity.prototype, "updatedAt", void 0);
__decorate([
    (0, typeorm_1.BeforeInsert)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], RecoverPasswordEntity.prototype, "defaultValues", null);
RecoverPasswordEntity = __decorate([
    (0, typeorm_1.Entity)({ name: 'recover_passwords' })
], RecoverPasswordEntity);
exports.RecoverPasswordEntity = RecoverPasswordEntity;
//# sourceMappingURL=recover-password.entity.js.map