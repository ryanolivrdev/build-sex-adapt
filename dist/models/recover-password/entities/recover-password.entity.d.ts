import { StatusType } from './../interfaces/status.type';
import { IRecoverPassword } from './../interfaces/recover-password.interface';
export declare class RecoverPasswordEntity implements IRecoverPassword {
    token: number;
    email: string;
    status: StatusType;
    createdAt: Date;
    updatedAt: Date;
    defaultValues(): void;
}
