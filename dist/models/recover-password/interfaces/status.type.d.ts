export declare type StatusType = 'PENDING' | 'CHANGING' | 'CONFIRMED';
