"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccessibilityEntity = void 0;
const openapi = require("@nestjs/swagger");
const base_entity_entity_1 = require("../../base/entities/base-entity.entity");
const establishment_entity_1 = require("../../establishment/entities/establishment.entity");
const user_entity_1 = require("../../user/entities/user.entity");
const typeorm_1 = require("typeorm");
let AccessibilityEntity = class AccessibilityEntity extends base_entity_entity_1.BaseEntity {
    static _OPENAPI_METADATA_FACTORY() {
        return { user: { required: true, type: () => require("../../user/entities/user.entity").UserEntity, nullable: true }, establishment: { required: true, type: () => require("../../establishment/entities/establishment.entity").EstablishmentEntity, nullable: true }, elevator: { required: true, type: () => Boolean }, bar: { required: true, type: () => Boolean }, uneeveness: { required: true, type: () => Boolean }, incompatible_dimensions: { required: true, type: () => Boolean }, sign_language: { required: true, type: () => Boolean }, tactile_floor: { required: true, type: () => Boolean }, braille: { required: true, type: () => Boolean } };
    }
};
__decorate([
    (0, typeorm_1.OneToOne)(() => user_entity_1.UserEntity, (user) => user.accessibilities, {
        onDelete: 'CASCADE',
        nullable: true
    }),
    (0, typeorm_1.JoinColumn)({ name: 'user_id' }),
    __metadata("design:type", user_entity_1.UserEntity)
], AccessibilityEntity.prototype, "user", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(() => establishment_entity_1.EstablishmentEntity, (establishment) => establishment.accessibilities, {
        onDelete: 'CASCADE',
        nullable: true
    }),
    __metadata("design:type", establishment_entity_1.EstablishmentEntity)
], AccessibilityEntity.prototype, "establishment", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Boolean)
], AccessibilityEntity.prototype, "elevator", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Boolean)
], AccessibilityEntity.prototype, "bar", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Boolean)
], AccessibilityEntity.prototype, "uneeveness", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Boolean)
], AccessibilityEntity.prototype, "incompatible_dimensions", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Boolean)
], AccessibilityEntity.prototype, "sign_language", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Boolean)
], AccessibilityEntity.prototype, "tactile_floor", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Boolean)
], AccessibilityEntity.prototype, "braille", void 0);
AccessibilityEntity = __decorate([
    (0, typeorm_1.Entity)({ name: 'accessibilities' })
], AccessibilityEntity);
exports.AccessibilityEntity = AccessibilityEntity;
//# sourceMappingURL=accessibility.entity.js.map