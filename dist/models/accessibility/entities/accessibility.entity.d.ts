import { BaseEntity } from '@models/base/entities/base-entity.entity';
import { EstablishmentEntity } from '@models/establishment/entities/establishment.entity';
import { UserEntity } from '@models/user/entities/user.entity';
import { IAccessibilityEntity } from '../interfaces/accessibility.interface';
export declare class AccessibilityEntity extends BaseEntity implements IAccessibilityEntity {
    user: UserEntity | null;
    establishment: EstablishmentEntity | null;
    elevator: boolean;
    bar: boolean;
    uneeveness: boolean;
    incompatible_dimensions: boolean;
    sign_language: boolean;
    tactile_floor: boolean;
    braille: boolean;
}
