"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateAccessibilityDto = void 0;
const openapi = require("@nestjs/swagger");
const class_validator_1 = require("class-validator");
class CreateAccessibilityDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { elevator: { required: true, type: () => Boolean }, bar: { required: true, type: () => Boolean }, uneeveness: { required: true, type: () => Boolean }, incompatible_dimensions: { required: true, type: () => Boolean }, sign_language: { required: true, type: () => Boolean }, tactile_floor: { required: true, type: () => Boolean }, braille: { required: true, type: () => Boolean } };
    }
}
__decorate([
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], CreateAccessibilityDto.prototype, "elevator", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], CreateAccessibilityDto.prototype, "bar", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], CreateAccessibilityDto.prototype, "uneeveness", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], CreateAccessibilityDto.prototype, "incompatible_dimensions", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], CreateAccessibilityDto.prototype, "sign_language", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], CreateAccessibilityDto.prototype, "tactile_floor", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)(),
    __metadata("design:type", Boolean)
], CreateAccessibilityDto.prototype, "braille", void 0);
exports.CreateAccessibilityDto = CreateAccessibilityDto;
//# sourceMappingURL=create-accessibility.dto.js.map