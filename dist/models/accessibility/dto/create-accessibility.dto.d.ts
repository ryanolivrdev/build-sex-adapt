export declare class CreateAccessibilityDto {
    elevator: boolean;
    bar: boolean;
    uneeveness: boolean;
    incompatible_dimensions: boolean;
    sign_language: boolean;
    tactile_floor: boolean;
    braille: boolean;
}
