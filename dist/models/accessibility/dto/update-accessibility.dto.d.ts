import { CreateAccessibilityDto } from './create-accessibility.dto';
declare const UpdateAccessibilityDto_base: import("@nestjs/mapped-types").MappedType<Partial<CreateAccessibilityDto>>;
export declare class UpdateAccessibilityDto extends UpdateAccessibilityDto_base {
}
export {};
