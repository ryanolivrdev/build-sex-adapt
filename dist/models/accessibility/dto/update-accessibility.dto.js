"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateAccessibilityDto = void 0;
const openapi = require("@nestjs/swagger");
const mapped_types_1 = require("@nestjs/mapped-types");
const create_accessibility_dto_1 = require("./create-accessibility.dto");
class UpdateAccessibilityDto extends (0, mapped_types_1.PartialType)(create_accessibility_dto_1.CreateAccessibilityDto) {
    static _OPENAPI_METADATA_FACTORY() {
        return {};
    }
}
exports.UpdateAccessibilityDto = UpdateAccessibilityDto;
//# sourceMappingURL=update-accessibility.dto.js.map