"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const openapi = require("@nestjs/swagger");
const favorite_service_1 = require("./../../providers/favorite/favorite.service");
const common_1 = require("@nestjs/common");
const user_service_1 = require("./user.service");
const update_user_dto_1 = require("./dto/update-user.dto");
const decorators_1 = require("../../common/decorators");
const suggestion_service_1 = require("../../providers/suggestion/suggestion.service");
const swagger_1 = require("@nestjs/swagger");
let UserController = class UserController {
    constructor(userService, favoriteService, suggestionService) {
        this.userService = userService;
        this.favoriteService = favoriteService;
        this.suggestionService = suggestionService;
    }
    update(id, updateUserDto) {
        return this.userService.update(id, updateUserDto);
    }
    removeById(id) {
        return this.userService.removeById(id);
    }
    async generateSuggestions(userId) {
        return await this.suggestionService.generateUserSuggestions(userId);
    }
    async getFavorites(userId) {
        return await this.favoriteService.getUserFavorites(userId);
    }
    async favorite(userId, establishmentId) {
        return await this.favoriteService.favorite(userId, establishmentId);
    }
    async unfavorite(userId, establishmentId) {
        return await this.favoriteService.unfavorite(userId, establishmentId);
    }
};
__decorate([
    (0, common_1.Patch)(':id'),
    openapi.ApiResponse({ status: 200, type: require("./entities/user.entity").UserEntity }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_user_dto_1.UpdateUserDto]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)('/deleteById/:id'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], UserController.prototype, "removeById", null);
__decorate([
    (0, common_1.Get)('/suggestion'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, (0, decorators_1.GetCurrentUserId)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "generateSuggestions", null);
__decorate([
    (0, common_1.Get)('/favorites'),
    openapi.ApiResponse({ status: 200, type: [require("../establishment/entities/establishment.entity").EstablishmentEntity] }),
    __param(0, (0, decorators_1.GetCurrentUserId)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getFavorites", null);
__decorate([
    (0, common_1.Post)('/favorites/:id'),
    openapi.ApiResponse({ status: 201, type: require("./entities/user.entity").UserEntity }),
    __param(0, (0, decorators_1.GetCurrentUserId)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "favorite", null);
__decorate([
    (0, common_1.Delete)('/favorites/:id'),
    openapi.ApiResponse({ status: 200, type: require("./entities/user.entity").UserEntity }),
    __param(0, (0, decorators_1.GetCurrentUserId)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "unfavorite", null);
UserController = __decorate([
    (0, swagger_1.ApiTags)('User Routes'),
    (0, common_1.Controller)('user'),
    __metadata("design:paramtypes", [user_service_1.UserService,
        favorite_service_1.FavoriteService,
        suggestion_service_1.SuggestionService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map