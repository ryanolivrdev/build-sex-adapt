import { AccessibilityEntity } from './../../accessibility/entities/accessibility.entity';
import { ReviewEntity } from './../../review/entities/review.entity';
import { BaseEntity } from './../../base/entities/base-entity.entity';
import { IUserEntity } from './../interfaces/user.interface';
import { SuportEntity } from '@models/suport/entities/suport.entity';
import { EstablishmentEntity } from '../../establishment/entities/establishment.entity';
export declare class UserEntity extends BaseEntity implements IUserEntity {
    email: string;
    password: string;
    name: string;
    accessibilities: AccessibilityEntity;
    reviews: ReviewEntity[];
    suports: SuportEntity[];
    favorites: EstablishmentEntity[];
    hashedRefreshToken: string;
}
