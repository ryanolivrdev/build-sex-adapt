"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserEntity = void 0;
const openapi = require("@nestjs/swagger");
const accessibility_entity_1 = require("./../../accessibility/entities/accessibility.entity");
const typeorm_1 = require("typeorm");
const review_entity_1 = require("./../../review/entities/review.entity");
const base_entity_entity_1 = require("./../../base/entities/base-entity.entity");
const suport_entity_1 = require("../../suport/entities/suport.entity");
const establishment_entity_1 = require("../../establishment/entities/establishment.entity");
let UserEntity = class UserEntity extends base_entity_entity_1.BaseEntity {
    static _OPENAPI_METADATA_FACTORY() {
        return { email: { required: true, type: () => String }, password: { required: true, type: () => String }, name: { required: true, type: () => String }, accessibilities: { required: true, type: () => require("../../accessibility/entities/accessibility.entity").AccessibilityEntity }, reviews: { required: true, type: () => [require("../../review/entities/review.entity").ReviewEntity] }, suports: { required: true, type: () => [require("../../suport/entities/suport.entity").SuportEntity] }, favorites: { required: true, type: () => [require("../../establishment/entities/establishment.entity").EstablishmentEntity] }, hashedRefreshToken: { required: true, type: () => String } };
    }
};
__decorate([
    (0, typeorm_1.Column)({ unique: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "email", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: '200' }),
    __metadata("design:type", String)
], UserEntity.prototype, "password", void 0);
__decorate([
    (0, typeorm_1.Column)({ length: 120, type: 'varchar' }),
    __metadata("design:type", String)
], UserEntity.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.OneToOne)(() => accessibility_entity_1.AccessibilityEntity, (accessibilities) => accessibilities.user, {
        cascade: true
    }),
    (0, typeorm_1.JoinColumn)({ name: 'accessibility_id' }),
    __metadata("design:type", accessibility_entity_1.AccessibilityEntity)
], UserEntity.prototype, "accessibilities", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => review_entity_1.ReviewEntity, (review) => review.user, { cascade: true }),
    (0, typeorm_1.JoinColumn)({ name: 'review_id' }),
    __metadata("design:type", Array)
], UserEntity.prototype, "reviews", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => suport_entity_1.SuportEntity, (suport) => suport.user, { cascade: true }),
    (0, typeorm_1.JoinColumn)({ name: 'suport_id' }),
    __metadata("design:type", Array)
], UserEntity.prototype, "suports", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => establishment_entity_1.EstablishmentEntity, (establishment) => establishment.favoritedBy, { cascade: true }),
    (0, typeorm_1.JoinTable)({ name: 'favorites' }),
    __metadata("design:type", Array)
], UserEntity.prototype, "favorites", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", String)
], UserEntity.prototype, "hashedRefreshToken", void 0);
UserEntity = __decorate([
    (0, typeorm_1.Entity)({ name: 'users' })
], UserEntity);
exports.UserEntity = UserEntity;
//# sourceMappingURL=user.entity.js.map