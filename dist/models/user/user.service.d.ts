import { AccessibilityEntity } from './../accessibility/entities/accessibility.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
export declare class UserService {
    private usersRepository;
    private accessibilityRepository;
    constructor(usersRepository: Repository<UserEntity>, accessibilityRepository: Repository<AccessibilityEntity>);
    create(createUserDto: CreateUserDto): Promise<UserEntity>;
    findAll(): Promise<UserEntity[]>;
    findOneById(id: string): Promise<UserEntity>;
    findOneByEmail(email: string): Promise<UserEntity>;
    findOneOrFail(options: FindOneOptions<UserEntity>): Promise<UserEntity>;
    update(id: string, updateUserDto: UpdateUserDto): Promise<UserEntity>;
    removeById(id: string): Promise<import("typeorm").DeleteResult>;
    removeByEmail(email: string): Promise<import("typeorm").DeleteResult>;
}
