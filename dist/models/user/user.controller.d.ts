import { FavoriteService } from './../../providers/favorite/favorite.service';
import { UserService } from './user.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { SuggestionService } from '@providers/suggestion/suggestion.service';
export declare class UserController {
    private readonly userService;
    private readonly favoriteService;
    private readonly suggestionService;
    constructor(userService: UserService, favoriteService: FavoriteService, suggestionService: SuggestionService);
    update(id: string, updateUserDto: UpdateUserDto): Promise<import("./entities/user.entity").UserEntity>;
    removeById(id: string): Promise<import("typeorm").DeleteResult>;
    generateSuggestions(userId: string): Promise<{
        stars: number;
        accessibilities: import("../accessibility/entities/accessibility.entity").AccessibilityEntity;
        favoritedBy: import("./entities/user.entity").UserEntity[];
        name: string;
        price: number;
        category: string;
        website?: string;
        address: {
            street: string;
            number: string;
            complement: string;
            cep: string;
        };
        ground_floor_room: boolean;
        latitude: number;
        longitude: number;
        cover_photo: string;
        room_photos?: string[];
        landline: string;
        whatsapp?: string;
        reviews: import("../review/entities/review.entity").ReviewEntity[];
        id: string;
        createdAt: Date;
        updatedAt: Date;
    }[]>;
    getFavorites(userId: string): Promise<import("../establishment/entities/establishment.entity").EstablishmentEntity[]>;
    favorite(userId: string, establishmentId: string): Promise<import("./entities/user.entity").UserEntity>;
    unfavorite(userId: string, establishmentId: string): Promise<import("./entities/user.entity").UserEntity>;
}
