"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const accessibility_entity_1 = require("./../accessibility/entities/accessibility.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("./entities/user.entity");
const argon2_1 = require("argon2");
let UserService = class UserService {
    constructor(usersRepository, accessibilityRepository) {
        this.usersRepository = usersRepository;
        this.accessibilityRepository = accessibilityRepository;
    }
    async create(createUserDto) {
        try {
            createUserDto.password = await (0, argon2_1.hash)(createUserDto.password);
            const user = this.usersRepository.create(createUserDto);
            const accessibility = this.accessibilityRepository.create(createUserDto.accessibilities);
            user.accessibilities = accessibility;
            await this.accessibilityRepository.save(accessibility);
            const saved = await this.usersRepository.save(user);
            delete saved.password;
            delete saved.createdAt;
            delete saved.updatedAt;
            delete saved.hashedRefreshToken;
            delete saved.accessibilities.id;
            delete saved.accessibilities.updatedAt;
            delete saved.accessibilities.createdAt;
            return saved;
        }
        catch (error) {
            throw new common_1.UnauthorizedException('E-mail already in use. Try to login');
        }
    }
    async findAll() {
        return await this.usersRepository.find({
            relations: ['reviews', 'accessibilities'],
            select: {
                email: true,
                id: true,
                name: true,
                reviews: {
                    comment: true,
                    grade: true,
                    id: true
                },
                suports: {
                    id: true,
                    createdAt: true,
                    message: true
                }
            }
        });
    }
    async findOneById(id) {
        return await this.usersRepository.findOne({
            where: {
                id
            },
            relations: ['reviews', 'accessibilities', 'suports', 'favorites']
        });
    }
    async findOneByEmail(email) {
        return await this.usersRepository.findOne({
            where: {
                email
            },
            relations: ['reviews', 'accessibilities', 'suports', 'favorites']
        });
    }
    async findOneOrFail(options) {
        try {
            return await this.usersRepository.findOneOrFail(Object.assign(Object.assign({}, options), { relations: ['reviews', 'accessibilities', 'suports', 'favorites'] }));
        }
        catch (error) {
            throw new common_1.NotFoundException(error.message);
        }
    }
    async update(id, updateUserDto) {
        if (updateUserDto.password) {
            updateUserDto.password = await (0, argon2_1.hash)(updateUserDto.password);
        }
        const user = await this.findOneOrFail({ where: { id } });
        this.usersRepository.merge(user, updateUserDto);
        return await this.usersRepository.save(user);
    }
    async removeById(id) {
        return await this.usersRepository.delete({ id });
    }
    async removeByEmail(email) {
        return await this.usersRepository.delete({ email });
    }
};
UserService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(user_entity_1.UserEntity)),
    __param(1, (0, typeorm_1.InjectRepository)(accessibility_entity_1.AccessibilityEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map