"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateUserDto = void 0;
const openapi = require("@nestjs/swagger");
const index_messages_1 = require("./../../../common/helpers/exceptions/messages/index.messages");
const class_validator_1 = require("class-validator");
const class_transformer_1 = require("class-transformer");
const create_accessibility_dto_1 = require("../../accessibility/dto/create-accessibility.dto");
class CreateUserDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { email: { required: true, type: () => String }, password: { required: true, type: () => String }, name: { required: true, type: () => String }, accessibilities: { required: true, type: () => require("../../accessibility/dto/create-accessibility.dto").CreateAccessibilityDto }, hashedRefreshToken: { required: true, type: () => String, nullable: true }, favorites: { required: true, type: () => [require("../../establishment/entities/establishment.entity").EstablishmentEntity], nullable: true } };
    }
}
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: index_messages_1.HttpCustomMessages.VALIDATION.EMAIL.INVALID }),
    (0, class_validator_1.IsString)({ message: index_messages_1.HttpCustomMessages.VALIDATION.EMAIL.INVALID }),
    (0, class_validator_1.IsEmail)({ message: index_messages_1.HttpCustomMessages.VALIDATION.EMAIL.REQUIRED }),
    __metadata("design:type", String)
], CreateUserDto.prototype, "email", void 0);
__decorate([
    (0, class_validator_1.IsString)({ message: index_messages_1.HttpCustomMessages.VALIDATION.PASSWORD.INVALID }),
    (0, class_validator_1.IsNotEmpty)({ message: index_messages_1.HttpCustomMessages.VALIDATION.PASSWORD.REQUIRED }),
    (0, class_validator_1.Matches)(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/, {
        message: index_messages_1.HttpCustomMessages.VALIDATION.PASSWORD.WEAK
    }),
    __metadata("design:type", String)
], CreateUserDto.prototype, "password", void 0);
__decorate([
    (0, class_validator_1.IsString)({ message: index_messages_1.HttpCustomMessages.VALIDATION.NAME.INVALID }),
    (0, class_validator_1.IsNotEmpty)({ message: index_messages_1.HttpCustomMessages.VALIDATION.NAME.REQUIRED }),
    __metadata("design:type", String)
], CreateUserDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsDefined)(),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.ValidateNested)(),
    (0, class_transformer_1.Type)(() => create_accessibility_dto_1.CreateAccessibilityDto),
    __metadata("design:type", create_accessibility_dto_1.CreateAccessibilityDto)
], CreateUserDto.prototype, "accessibilities", void 0);
exports.CreateUserDto = CreateUserDto;
//# sourceMappingURL=create-user.dto.js.map