import { CreateAccessibilityDto } from '@models/accessibility/dto/create-accessibility.dto';
import { EstablishmentEntity } from '@models/establishment/entities/establishment.entity';
export declare class CreateUserDto {
    email: string;
    password: string;
    name: string;
    accessibilities: CreateAccessibilityDto;
    hashedRefreshToken: string | null;
    favorites: null | EstablishmentEntity[];
}
