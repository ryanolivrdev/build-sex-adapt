import { EstablishmentService } from './establishment.service';
import { CreateEstablishmentDto } from './dto/create-establishment.dto';
import { UpdateEstablishmentDto } from './dto/update-establishment.dto';
export declare class EstablishmentController {
    private readonly establishmentService;
    constructor(establishmentService: EstablishmentService);
    create(createEstablishmentDto: CreateEstablishmentDto): Promise<import("./entities/establishment.entity").EstablishmentEntity>;
    findAll(): Promise<{
        stars: number;
        accessibilities: import("../accessibility/entities/accessibility.entity").AccessibilityEntity;
        favoritedBy: import("../user/entities/user.entity").UserEntity[];
        name: string;
        price: number;
        category: string;
        website?: string;
        address: {
            street: string;
            number: string;
            complement: string;
            cep: string;
        };
        ground_floor_room: boolean;
        latitude: number;
        longitude: number;
        cover_photo: string;
        room_photos?: string[];
        landline: string;
        whatsapp?: string;
        reviews: import("../review/entities/review.entity").ReviewEntity[];
        id: string;
        createdAt: Date;
        updatedAt: Date;
    }[]>;
    findOne(id: string): Promise<{
        stars: number;
        accessibilities: import("../accessibility/entities/accessibility.entity").AccessibilityEntity;
        favoritedBy: import("../user/entities/user.entity").UserEntity[];
        name: string;
        price: number;
        category: string;
        website?: string;
        address: {
            street: string;
            number: string;
            complement: string;
            cep: string;
        };
        ground_floor_room: boolean;
        latitude: number;
        longitude: number;
        cover_photo: string;
        room_photos?: string[];
        landline: string;
        whatsapp?: string;
        reviews: import("../review/entities/review.entity").ReviewEntity[];
        id: string;
        createdAt: Date;
        updatedAt: Date;
    }>;
    update(id: string, updateEstablishmentDto: UpdateEstablishmentDto): Promise<{
        stars: number;
        accessibilities: import("../accessibility/entities/accessibility.entity").AccessibilityEntity;
        favoritedBy: import("../user/entities/user.entity").UserEntity[];
        name: string;
        price: number;
        category: string;
        website?: string;
        address: {
            street: string;
            number: string;
            complement: string;
            cep: string;
        };
        ground_floor_room: boolean;
        latitude: number;
        longitude: number;
        cover_photo: string;
        room_photos?: string[];
        landline: string;
        whatsapp?: string;
        reviews: import("../review/entities/review.entity").ReviewEntity[];
        id: string;
        createdAt: Date;
        updatedAt: Date;
    } & import("./entities/establishment.entity").EstablishmentEntity>;
    remove(id: string): Promise<import("typeorm").DeleteResult>;
}
