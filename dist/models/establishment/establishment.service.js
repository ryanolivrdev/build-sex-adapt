"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EstablishmentService = void 0;
const accessibility_entity_1 = require("../accessibility/entities/accessibility.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const establishment_entity_1 = require("./entities/establishment.entity");
const index_messages_1 = require("../../common/helpers/exceptions/messages/index.messages");
const getEstablishmentStars_1 = require("../../utils/getEstablishmentStars");
let EstablishmentService = class EstablishmentService {
    constructor(establishmentRepository, accessibilityRepository) {
        this.establishmentRepository = establishmentRepository;
        this.accessibilityRepository = accessibilityRepository;
    }
    async create(createEstablishmentDto) {
        const establishment = this.establishmentRepository.create(createEstablishmentDto);
        const accessibility = this.accessibilityRepository.create(Object.assign(Object.assign({}, createEstablishmentDto.accessibilities), { establishment: establishment }));
        establishment.accessibilities = accessibility;
        await this.accessibilityRepository.save(accessibility);
        return await this.establishmentRepository.save(establishment);
    }
    async findAll() {
        const establishments = await this.establishmentRepository.find({
            relations: ['accessibilities', 'favorites', 'reviews']
        });
        const establishmentsWithStars = establishments.map((establishment) => {
            return Object.assign(Object.assign({}, establishment), { stars: (0, getEstablishmentStars_1.getEstablishmentStars)(establishment) });
        });
        return establishmentsWithStars;
    }
    async findByAccessibilities(accessibilities) {
        const establishments = await this.establishmentRepository.find({
            where: {
                accessibilities: {
                    bar: accessibilities.bar,
                    elevator: accessibilities.elevator,
                    incompatible_dimensions: accessibilities.incompatible_dimensions,
                    sign_language: accessibilities.sign_language,
                    braille: accessibilities.braille,
                    tactile_floor: accessibilities.tactile_floor,
                    uneeveness: accessibilities.uneeveness
                }
            },
            relations: ['accessibilities', 'favorites', 'reviews']
        });
        const establishmentsWithStars = establishments.map((establishment) => {
            return Object.assign(Object.assign({}, establishment), { stars: (0, getEstablishmentStars_1.getEstablishmentStars)(establishment) });
        });
        return establishmentsWithStars;
    }
    async findOneOrFail(options) {
        try {
            const establishment = await this.establishmentRepository.findOneOrFail(Object.assign(Object.assign({}, options), { relations: ['accessibilities', 'favorites', 'reviews'], select: {
                    accessibilities: {
                        bar: true,
                        braille: true,
                        elevator: true,
                        incompatible_dimensions: true,
                        sign_language: true,
                        tactile_floor: true,
                        uneeveness: true
                    }
                } }));
            return Object.assign(Object.assign({}, establishment), { stars: (0, getEstablishmentStars_1.getEstablishmentStars)(establishment) });
        }
        catch (error) {
            throw new common_1.NotFoundException(index_messages_1.HttpCustomMessages.ESTABLISHMENT.NOT_FOUND);
        }
    }
    async update(id, updateEstablishmentDto) {
        const establishment = await this.findOneOrFail({ where: { id } });
        this.establishmentRepository.merge(establishment, updateEstablishmentDto);
        return await this.establishmentRepository.save(establishment);
    }
    async remove(id) {
        return await this.establishmentRepository.delete({ id });
    }
};
EstablishmentService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(establishment_entity_1.EstablishmentEntity)),
    __param(1, (0, typeorm_1.InjectRepository)(accessibility_entity_1.AccessibilityEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], EstablishmentService);
exports.EstablishmentService = EstablishmentService;
//# sourceMappingURL=establishment.service.js.map