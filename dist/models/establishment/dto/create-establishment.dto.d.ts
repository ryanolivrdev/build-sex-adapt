import { CreateAccessibilityDto } from '@models/accessibility/dto/create-accessibility.dto';
import { UserEntity } from '../../user/entities/user.entity';
export declare class CreateEstablishmentDto {
    accessibilities: CreateAccessibilityDto;
    favoritedBy: UserEntity[] | null;
    name: string;
    price: number;
    category: string;
    website: string;
    address: {
        street: string;
        number: string;
        complement: string;
        cep: string;
    };
    ground_floor_room: boolean;
    latitude: number;
    longitude: number;
    cover_photo: string;
    room_photo: string[];
    landline: string;
    whatsapp: string;
}
