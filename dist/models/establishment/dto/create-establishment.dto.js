"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateEstablishmentDto = void 0;
const openapi = require("@nestjs/swagger");
const create_accessibility_dto_1 = require("../../accessibility/dto/create-accessibility.dto");
const class_transformer_1 = require("class-transformer");
const index_messages_1 = require("../../../common/helpers/exceptions/messages/index.messages");
const class_validator_1 = require("class-validator");
class CreateEstablishmentDto {
    static _OPENAPI_METADATA_FACTORY() {
        return { accessibilities: { required: true, type: () => require("../../accessibility/dto/create-accessibility.dto").CreateAccessibilityDto }, favoritedBy: { required: true, type: () => [require("../../user/entities/user.entity").UserEntity], nullable: true }, name: { required: true, type: () => String }, price: { required: true, type: () => Number }, category: { required: true, type: () => String }, website: { required: true, type: () => String }, address: { required: true, type: () => ({ street: { required: true, type: () => String }, number: { required: true, type: () => String }, complement: { required: true, type: () => String }, cep: { required: true, type: () => String } }) }, ground_floor_room: { required: true, type: () => Boolean }, latitude: { required: true, type: () => Number }, longitude: { required: true, type: () => Number }, cover_photo: { required: true, type: () => String }, room_photo: { required: true, type: () => [String] }, landline: { required: true, type: () => String }, whatsapp: { required: true, type: () => String } };
    }
}
__decorate([
    (0, class_validator_1.IsDefined)(),
    (0, class_validator_1.IsNotEmptyObject)(),
    (0, class_validator_1.IsObject)(),
    (0, class_validator_1.ValidateNested)(),
    (0, class_transformer_1.Type)(() => create_accessibility_dto_1.CreateAccessibilityDto),
    __metadata("design:type", create_accessibility_dto_1.CreateAccessibilityDto)
], CreateEstablishmentDto.prototype, "accessibilities", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: index_messages_1.HttpCustomMessages.VALIDATION.NAME.REQUIRED }),
    (0, class_validator_1.IsString)({ message: index_messages_1.HttpCustomMessages.VALIDATION.NAME.INVALID }),
    __metadata("design:type", String)
], CreateEstablishmentDto.prototype, "name", void 0);
__decorate([
    (0, class_validator_1.IsNumber)({ maxDecimalPlaces: 2 }, { message: index_messages_1.HttpCustomMessages.VALIDATION.PRICE.INVALID }),
    __metadata("design:type", Number)
], CreateEstablishmentDto.prototype, "price", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: index_messages_1.HttpCustomMessages.VALIDATION.CATEGORY.REQUIRED }),
    (0, class_validator_1.IsString)({ message: index_messages_1.HttpCustomMessages.VALIDATION.CATEGORY.INVALID }),
    __metadata("design:type", String)
], CreateEstablishmentDto.prototype, "category", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNotEmpty)({ message: index_messages_1.HttpCustomMessages.VALIDATION.WEBSITE.REQUIRED }),
    (0, class_validator_1.IsString)({ message: index_messages_1.HttpCustomMessages.VALIDATION.WEBSITE.INVALID }),
    __metadata("design:type", String)
], CreateEstablishmentDto.prototype, "website", void 0);
__decorate([
    (0, class_validator_1.IsObject)({ message: index_messages_1.HttpCustomMessages.VALIDATION.ADDRESS.INVALID }),
    (0, class_validator_1.IsDefined)(),
    (0, class_validator_1.IsNotEmptyObject)({ nullable: false }, { message: index_messages_1.HttpCustomMessages.VALIDATION.ADDRESS.REQUIRED }),
    (0, class_validator_1.ValidateNested)({ each: true }),
    __metadata("design:type", Object)
], CreateEstablishmentDto.prototype, "address", void 0);
__decorate([
    (0, class_validator_1.IsBoolean)({
        message: index_messages_1.HttpCustomMessages.VALIDATION.GROUND_FLOOR_ROOM.INVALID
    }),
    __metadata("design:type", Boolean)
], CreateEstablishmentDto.prototype, "ground_floor_room", void 0);
__decorate([
    (0, class_validator_1.IsLatitude)(),
    __metadata("design:type", Number)
], CreateEstablishmentDto.prototype, "latitude", void 0);
__decorate([
    (0, class_validator_1.IsLongitude)(),
    __metadata("design:type", Number)
], CreateEstablishmentDto.prototype, "longitude", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], CreateEstablishmentDto.prototype, "cover_photo", void 0);
__decorate([
    (0, class_validator_1.IsNotEmpty)(),
    (0, class_validator_1.IsArray)(),
    __metadata("design:type", Array)
], CreateEstablishmentDto.prototype, "room_photo", void 0);
__decorate([
    (0, class_validator_1.Matches)(/\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})? ?(\w{1,10}\s?\d{1,6})?/, { message: index_messages_1.HttpCustomMessages.VALIDATION.LANDLINE.INVALID }),
    __metadata("design:type", String)
], CreateEstablishmentDto.prototype, "landline", void 0);
__decorate([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsNotEmpty)({ message: index_messages_1.HttpCustomMessages.VALIDATION.WHATSAPP.REQUIRED }),
    (0, class_validator_1.IsString)({ message: index_messages_1.HttpCustomMessages.VALIDATION.WHATSAPP.INVALID }),
    __metadata("design:type", String)
], CreateEstablishmentDto.prototype, "whatsapp", void 0);
exports.CreateEstablishmentDto = CreateEstablishmentDto;
//# sourceMappingURL=create-establishment.dto.js.map