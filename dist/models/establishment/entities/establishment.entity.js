"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EstablishmentEntity = void 0;
const openapi = require("@nestjs/swagger");
const accessibility_entity_1 = require("../../accessibility/entities/accessibility.entity");
const typeorm_1 = require("typeorm");
const base_entity_entity_1 = require("../../base/entities/base-entity.entity");
const user_entity_1 = require("../../user/entities/user.entity");
const review_entity_1 = require("../../review/entities/review.entity");
let EstablishmentEntity = class EstablishmentEntity extends base_entity_entity_1.BaseEntity {
    static _OPENAPI_METADATA_FACTORY() {
        return { accessibilities: { required: true, type: () => require("../../accessibility/entities/accessibility.entity").AccessibilityEntity }, favoritedBy: { required: true, type: () => [require("../../user/entities/user.entity").UserEntity] }, name: { required: true, type: () => String }, price: { required: true, type: () => Number }, category: { required: true, type: () => String }, website: { required: false, type: () => String }, address: { required: true, type: () => ({ street: { required: true, type: () => String }, number: { required: true, type: () => String }, complement: { required: true, type: () => String }, cep: { required: true, type: () => String } }) }, ground_floor_room: { required: true, type: () => Boolean }, latitude: { required: true, type: () => Number }, longitude: { required: true, type: () => Number }, cover_photo: { required: true, type: () => String }, room_photos: { required: false, type: () => [String] }, landline: { required: true, type: () => String }, whatsapp: { required: false, type: () => String }, reviews: { required: true, type: () => [require("../../review/entities/review.entity").ReviewEntity] } };
    }
};
__decorate([
    (0, typeorm_1.OneToOne)(() => accessibility_entity_1.AccessibilityEntity, (accessibilities) => accessibilities.establishment, {
        cascade: true
    }),
    (0, typeorm_1.JoinColumn)({ name: 'accessibility_id' }),
    __metadata("design:type", accessibility_entity_1.AccessibilityEntity)
], EstablishmentEntity.prototype, "accessibilities", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => user_entity_1.UserEntity, (user) => user.favorites),
    __metadata("design:type", Array)
], EstablishmentEntity.prototype, "favoritedBy", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], EstablishmentEntity.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'float' }),
    __metadata("design:type", Number)
], EstablishmentEntity.prototype, "price", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], EstablishmentEntity.prototype, "category", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: '' }),
    __metadata("design:type", String)
], EstablishmentEntity.prototype, "website", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'json' }),
    __metadata("design:type", Object)
], EstablishmentEntity.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Boolean)
], EstablishmentEntity.prototype, "ground_floor_room", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", Number)
], EstablishmentEntity.prototype, "latitude", void 0);
__decorate([
    (0, typeorm_1.Column)({ nullable: true }),
    __metadata("design:type", Number)
], EstablishmentEntity.prototype, "longitude", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], EstablishmentEntity.prototype, "cover_photo", void 0);
__decorate([
    (0, typeorm_1.Column)('text', { array: true, default: [] }),
    __metadata("design:type", Array)
], EstablishmentEntity.prototype, "room_photos", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], EstablishmentEntity.prototype, "landline", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: '' }),
    __metadata("design:type", String)
], EstablishmentEntity.prototype, "whatsapp", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => review_entity_1.ReviewEntity, (review) => review.establishment, {
        eager: true
    }),
    __metadata("design:type", Array)
], EstablishmentEntity.prototype, "reviews", void 0);
EstablishmentEntity = __decorate([
    (0, typeorm_1.Entity)({ name: 'establishments' })
], EstablishmentEntity);
exports.EstablishmentEntity = EstablishmentEntity;
//# sourceMappingURL=establishment.entity.js.map