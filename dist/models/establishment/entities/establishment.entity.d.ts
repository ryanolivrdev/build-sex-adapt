import { AccessibilityEntity } from '@models/accessibility/entities/accessibility.entity';
import { BaseEntity } from '../../base/entities/base-entity.entity';
import { IEstablishmentEntity } from '../interfaces/establishment.interface';
import { UserEntity } from '../../user/entities/user.entity';
import { ReviewEntity } from '../../review/entities/review.entity';
export declare class EstablishmentEntity extends BaseEntity implements IEstablishmentEntity {
    accessibilities: AccessibilityEntity;
    favoritedBy: UserEntity[];
    name: string;
    price: number;
    category: string;
    website?: string;
    address: {
        street: string;
        number: string;
        complement: string;
        cep: string;
    };
    ground_floor_room: boolean;
    latitude: number;
    longitude: number;
    cover_photo: string;
    room_photos?: string[];
    landline: string;
    whatsapp?: string;
    reviews: ReviewEntity[];
}
