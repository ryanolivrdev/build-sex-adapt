"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EstablishmentController = void 0;
const openapi = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const establishment_service_1 = require("./establishment.service");
const create_establishment_dto_1 = require("./dto/create-establishment.dto");
const update_establishment_dto_1 = require("./dto/update-establishment.dto");
const _1 = require("../../common/decorators/index");
const swagger_1 = require("@nestjs/swagger");
let EstablishmentController = class EstablishmentController {
    constructor(establishmentService) {
        this.establishmentService = establishmentService;
    }
    async create(createEstablishmentDto) {
        return await this.establishmentService.create(createEstablishmentDto);
    }
    findAll() {
        return this.establishmentService.findAll();
    }
    findOne(id) {
        return this.establishmentService.findOneOrFail({ where: { id } });
    }
    update(id, updateEstablishmentDto) {
        return this.establishmentService.update(id, updateEstablishmentDto);
    }
    remove(id) {
        return this.establishmentService.remove(id);
    }
};
__decorate([
    (0, common_1.Post)(),
    openapi.ApiResponse({ status: 201, type: require("./entities/establishment.entity").EstablishmentEntity }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_establishment_dto_1.CreateEstablishmentDto]),
    __metadata("design:returntype", Promise)
], EstablishmentController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    openapi.ApiResponse({ status: 200 }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], EstablishmentController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], EstablishmentController.prototype, "findOne", null);
__decorate([
    (0, common_1.Patch)(':id'),
    openapi.ApiResponse({ status: 200, type: Object }),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_establishment_dto_1.UpdateEstablishmentDto]),
    __metadata("design:returntype", void 0)
], EstablishmentController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    openapi.ApiResponse({ status: 200 }),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], EstablishmentController.prototype, "remove", null);
EstablishmentController = __decorate([
    (0, swagger_1.ApiTags)('Establishment Routes'),
    (0, _1.Public)(),
    (0, common_1.Controller)('establishment'),
    __metadata("design:paramtypes", [establishment_service_1.EstablishmentService])
], EstablishmentController);
exports.EstablishmentController = EstablishmentController;
//# sourceMappingURL=establishment.controller.js.map