"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getEstablishmentStars = void 0;
function getEstablishmentStars(establishment) {
    let stars = 0;
    establishment.reviews.forEach((review) => {
        stars += review.grade;
    });
    return stars / establishment.reviews.length;
}
exports.getEstablishmentStars = getEstablishmentStars;
//# sourceMappingURL=getEstablishmentStars.js.map