import { EstablishmentEntity } from '../models/establishment/entities/establishment.entity';
export declare function getEstablishmentStars(establishment: EstablishmentEntity): number;
