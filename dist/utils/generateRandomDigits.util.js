"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateRandomDigits = void 0;
const generateRandomDigits = (from, to) => {
    return Math.floor(Math.random() * (to - from) + from);
};
exports.generateRandomDigits = generateRandomDigits;
//# sourceMappingURL=generateRandomDigits.util.js.map