import { UserEntity } from '@user/entities/user.entity';
import { MailerService } from '@nestjs-modules/mailer';
import { BaseEmail } from './@types/base-email.type';
export declare class MailService {
    private readonly mailerService;
    constructor(mailerService: MailerService);
    sendMail(user: UserEntity, email: BaseEmail): Promise<SentMessageInfo>;
    sendSuportMail(email: BaseEmail, from: string): Promise<SentMessageInfo>;
    mailRecoverToken(email: string, token: number): Promise<SentMessageInfo>;
}
