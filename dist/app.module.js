"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const typeorm_config_1 = require("./config/typeorm/typeorm.config");
const config_1 = require("@nestjs/config");
const common_1 = require("@nestjs/common");
const user_module_1 = require("./models/user/user.module");
const typeorm_1 = require("@nestjs/typeorm");
const auth_module_1 = require("./auth/auth.module");
const core_1 = require("@nestjs/core");
const access_token_guard_1 = require("./common/guards/access-token.guard");
const review_module_1 = require("./models/review/review.module");
const suport_module_1 = require("./models/suport/suport.module");
const establishment_module_1 = require("./models/establishment/establishment.module");
const favorite_module_1 = require("./providers/favorite/favorite.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            config_1.ConfigModule.forRoot({
                isGlobal: true
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                imports: [config_1.ConfigModule],
                inject: [config_1.ConfigService],
                useClass: typeorm_config_1.TypeOrmConfigService
            }),
            user_module_1.UserModule,
            auth_module_1.AuthModule,
            review_module_1.ReviewModule,
            suport_module_1.SuportModule,
            establishment_module_1.EstablishmentModule,
            favorite_module_1.FavoriteModule
        ],
        controllers: [],
        providers: [
            {
                provide: core_1.APP_GUARD,
                useClass: access_token_guard_1.AccessTokenGuard
            }
        ]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map