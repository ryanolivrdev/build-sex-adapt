"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const access_token_guard_1 = require("./common/guards/access-token.guard");
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const morgan = require("morgan");
const swagger_1 = require("@nestjs/swagger");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.useGlobalPipes(new common_1.ValidationPipe());
    app.useGlobalGuards(new access_token_guard_1.AccessTokenGuard(new core_1.Reflector()));
    morgan.token('body', (req) => {
        return JSON.stringify(req.body);
    });
    app.use(morgan(':method :url :status\n - :response-time ms -\n :body'));
    const config = new swagger_1.DocumentBuilder()
        .setTitle('Sex-adapt')
        .setDescription('API Routes and datas')
        .setVersion('1.0')
        .build();
    const document = swagger_1.SwaggerModule.createDocument(app, config);
    swagger_1.SwaggerModule.setup('docs', app, document);
    await app.listen(3000, () => {
        console.log('Listening on localhost:8080');
    });
}
bootstrap();
//# sourceMappingURL=main.js.map