import { CreateUserDto } from './../models/user/dto/create-user.dto';
import { AuthDto } from './dto/auth.dto';
import { AuthService } from './auth.service';
import { Tokens } from './@types/tokens.type';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    signup_local(createUserDto: CreateUserDto): Promise<{
        user: import("../models/user/entities/user.entity").UserEntity;
        tokens: Tokens;
    }>;
    signin_local(authDto: AuthDto): Promise<{
        user: import("../models/user/entities/user.entity").UserEntity;
        tokens: Tokens;
    }>;
    logout(userId: string): Promise<void>;
    refresh_token(userId: string, refreshToken: string): Promise<Tokens>;
}
