"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecoverController = void 0;
const openapi = require("@nestjs/swagger");
const _1 = require("../common/decorators/index");
const recover_password_1 = require("../models/recover-password");
const change_password_dto_1 = require("../models/recover-password/dto/change-password.dto");
const confirm_token_dto_1 = require("../models/recover-password/dto/confirm-token.dto");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const recover_password_service_1 = require("../providers/recover-password/recover-password.service");
let RecoverController = class RecoverController {
    constructor(recoverService) {
        this.recoverService = recoverService;
    }
    async recoverPassword(recoverPasswordDto) {
        return await this.recoverService.create(recoverPasswordDto);
    }
    async confirmToken(confirmTokenDto) {
        return await this.recoverService.confirmToken(confirmTokenDto);
    }
    async changePassword(changePasswordDto) {
        return await this.recoverService.changePassword(changePasswordDto);
    }
};
__decorate([
    (0, common_1.Post)(),
    (0, common_1.HttpCode)(common_1.HttpStatus.OK),
    openapi.ApiResponse({ status: common_1.HttpStatus.OK, type: Object }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [recover_password_1.CreateRecoverPasswordDto]),
    __metadata("design:returntype", Promise)
], RecoverController.prototype, "recoverPassword", null);
__decorate([
    (0, common_1.Post)('confirm'),
    (0, common_1.HttpCode)(common_1.HttpStatus.OK),
    openapi.ApiResponse({ status: common_1.HttpStatus.OK, type: Object }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [confirm_token_dto_1.ConfirmTokenDto]),
    __metadata("design:returntype", Promise)
], RecoverController.prototype, "confirmToken", null);
__decorate([
    (0, common_1.Put)('changePassword'),
    (0, common_1.HttpCode)(common_1.HttpStatus.OK),
    openapi.ApiResponse({ status: common_1.HttpStatus.OK, type: require("../models/user/entities/user.entity").UserEntity }),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [change_password_dto_1.ChangePasswordDto]),
    __metadata("design:returntype", Promise)
], RecoverController.prototype, "changePassword", null);
RecoverController = __decorate([
    (0, swagger_1.ApiTags)('Recover Routes'),
    (0, _1.Public)(),
    (0, common_1.Controller)('auth/recover'),
    __metadata("design:paramtypes", [recover_password_service_1.RecoverPasswordService])
], RecoverController);
exports.RecoverController = RecoverController;
//# sourceMappingURL=recover.controller.js.map