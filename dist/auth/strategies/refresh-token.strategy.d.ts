import { JwtPayloadWithRefreshToken } from '../@types/jwt-payload-with-refresh-token.type';
import { Strategy } from 'passport-jwt';
import { Request } from 'express';
import { ConfigService } from '@nestjs/config';
declare const RefreshTokenStrategy_base: new (...args: any[]) => Strategy;
export declare class RefreshTokenStrategy extends RefreshTokenStrategy_base {
    private readonly configService;
    constructor(configService: ConfigService);
    validate(req: Request, payload: JwtPayloadWithRefreshToken): {
        refreshToken: string;
        email: string;
        sub: string;
    };
}
export {};
