"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const login_failed_exception_1 = require("./../common/exceptions/login-failed.exception");
const not_found_exception_1 = require("./../common/exceptions/not-found.exception");
const config_1 = require("@nestjs/config");
const common_1 = require("@nestjs/common");
const argon2 = require("argon2");
const jwt_1 = require("@nestjs/jwt");
const user_service_1 = require("../models/user/user.service");
let AuthService = class AuthService {
    constructor(userService, jwtService, configService) {
        this.userService = userService;
        this.jwtService = jwtService;
        this.configService = configService;
    }
    async signup_local(userInfo) {
        const new_user = await this.userService.create(userInfo);
        const tokens = await this.getTokens(new_user.id, new_user.email);
        await this.updateRtHash(new_user.id, tokens.refresh_token);
        return { user: new_user, tokens };
    }
    async signin_local(authInfo) {
        const user = await this.userService.findOneByEmail(authInfo.email);
        if (!user)
            throw new login_failed_exception_1.LoginFailedException();
        const password_match = await argon2.verify(user.password, authInfo.password);
        if (!password_match)
            throw new login_failed_exception_1.LoginFailedException();
        const tokens = await this.getTokens(user.id, user.email);
        await this.updateRtHash(user.id, tokens.refresh_token);
        return { user, tokens };
    }
    async logout(userId) {
        await this.userService.update(userId, {
            hashedRefreshToken: null
        });
    }
    async updateRefreshToken(userId, refresh_token) {
        const user = await this.userService.findOneById(userId);
        if (!user || !user.hashedRefreshToken)
            throw new not_found_exception_1.NotFoundException('User not found');
        const rt_match = await argon2.verify(user.hashedRefreshToken, refresh_token);
        if (!rt_match)
            throw new common_1.UnauthorizedException();
        const tokens = await this.getTokens(user.id, user.email);
        await this.updateRtHash(user.id, tokens.refresh_token);
        return tokens;
    }
    async updateRtHash(userId, refresh_token) {
        const hash = await this.hashData(refresh_token);
        await this.userService.update(userId, {
            hashedRefreshToken: hash
        });
    }
    hashData(data) {
        return argon2.hash(data);
    }
    async getTokens(userId, email) {
        const jwtPayload = {
            sub: userId,
            email
        };
        const [access_token, refresh_token] = await Promise.all([
            this.jwtService.signAsync(jwtPayload, {
                secret: this.configService.get('JWT_ACCESS_SECRET'),
                expiresIn: '10h'
            }),
            this.jwtService.signAsync(jwtPayload, {
                secret: this.configService.get('JWT_REFRESH_SECRET'),
                expiresIn: '7d'
            })
        ]);
        return {
            refresh_token,
            access_token
        };
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_service_1.UserService,
        jwt_1.JwtService,
        config_1.ConfigService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map