import { CreateRecoverPasswordDto } from '@models/recover-password';
import { ChangePasswordDto } from '@models/recover-password/dto/change-password.dto';
import { ConfirmTokenDto } from '@models/recover-password/dto/confirm-token.dto';
import { RecoverPasswordService } from '@providers/recover-password/recover-password.service';
export declare class RecoverController {
    private recoverService;
    constructor(recoverService: RecoverPasswordService);
    recoverPassword(recoverPasswordDto: CreateRecoverPasswordDto): Promise<any>;
    confirmToken(confirmTokenDto: ConfirmTokenDto): Promise<import("../common/exceptions/token-invalid.exception").TokenInvalidException | import("typeorm").UpdateResult>;
    changePassword(changePasswordDto: ChangePasswordDto): Promise<import("../models/user/entities/user.entity").UserEntity>;
}
