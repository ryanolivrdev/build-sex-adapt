import { CreateUserDto } from './../models/user/dto/create-user.dto';
import { ConfigService } from '@nestjs/config';
import { AuthDto } from './dto/auth.dto';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '@models/user/user.service';
import { Tokens } from './@types/tokens.type';
export declare class AuthService {
    private userService;
    private jwtService;
    private readonly configService;
    constructor(userService: UserService, jwtService: JwtService, configService: ConfigService);
    signup_local(userInfo: CreateUserDto): Promise<{
        user: import("../models/user/entities/user.entity").UserEntity;
        tokens: Tokens;
    }>;
    signin_local(authInfo: AuthDto): Promise<{
        user: import("../models/user/entities/user.entity").UserEntity;
        tokens: Tokens;
    }>;
    logout(userId: string): Promise<void>;
    updateRefreshToken(userId: string, refresh_token: string): Promise<Tokens>;
    updateRtHash(userId: string, refresh_token: string): Promise<void>;
    hashData(data: string): Promise<string>;
    getTokens(userId: string, email: string): Promise<Tokens>;
}
