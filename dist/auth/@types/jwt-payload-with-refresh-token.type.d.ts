import { JwtPayload } from './jwt-payload.type';
export declare type JwtPayloadWithRefreshToken = JwtPayload & {
    refreshToken: string;
};
