export declare type JwtPayload = {
    email: string;
    sub: string;
};
