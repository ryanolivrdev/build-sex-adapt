import * as Joi from 'joi';
declare const _default: () => {
    envFilePath: string[];
    validationSchema: Joi.ObjectSchema<any>;
};
export default _default;
