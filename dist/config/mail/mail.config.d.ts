import { MailerOptions, MailerOptionsFactory } from '@nestjs-modules/mailer';
import { ConfigService } from '@nestjs/config';
export declare class MailerConfigService implements MailerOptionsFactory {
    private configService;
    constructor(configService: ConfigService);
    createMailerOptions(): MailerOptions | Promise<MailerOptions>;
}
