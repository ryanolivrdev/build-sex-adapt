"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MailerConfigService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const handlebars_adapter_1 = require("@nestjs-modules/mailer/dist/adapters/handlebars.adapter");
const path_1 = require("path");
let MailerConfigService = class MailerConfigService {
    constructor(configService) {
        this.configService = configService;
    }
    createMailerOptions() {
        return {
            transport: {
                service: this.configService.get('MAILER_SERVICE'),
                port: this.configService.get('MAILER_PORT'),
                secure: false,
                auth: {
                    user: this.configService.get('MAILER_USER'),
                    pass: this.configService.get('MAILER_PASSWORD')
                }
            },
            defaults: {
                from: 'SexAdapt <sex.adapt.sac@gmail.com>'
            },
            template: {
                dir: (0, path_1.join)(process.cwd(), 'src', 'services', 'mail', 'templates'),
                adapter: new handlebars_adapter_1.HandlebarsAdapter(),
                options: {
                    strict: true
                }
            }
        };
    }
};
MailerConfigService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService])
], MailerConfigService);
exports.MailerConfigService = MailerConfigService;
//# sourceMappingURL=mail.config.js.map