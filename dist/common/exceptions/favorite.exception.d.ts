import { HttpException } from '@nestjs/common';
export declare class FavoriteException extends HttpException {
    constructor(error: string);
}
