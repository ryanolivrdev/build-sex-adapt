"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenInvalidException = void 0;
const index_messages_1 = require("../helpers/exceptions/messages/index.messages");
const common_1 = require("@nestjs/common");
class TokenInvalidException extends common_1.HttpException {
    constructor() {
        super(index_messages_1.HttpCustomMessages.RECOVER.TOKEN.INVALID, common_1.HttpStatus.FORBIDDEN);
    }
}
exports.TokenInvalidException = TokenInvalidException;
//# sourceMappingURL=token-invalid.exception.js.map