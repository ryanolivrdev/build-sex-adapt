import { HttpException } from '@nestjs/common';
export declare class LoginFailedException extends HttpException {
    constructor();
}
