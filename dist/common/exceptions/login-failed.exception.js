"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoginFailedException = void 0;
const index_messages_1 = require("./../helpers/exceptions/messages/index.messages");
const common_1 = require("@nestjs/common");
class LoginFailedException extends common_1.HttpException {
    constructor() {
        super(index_messages_1.HttpCustomMessages.AUTHORIZATION.LOGIN_FAILED, common_1.HttpStatus.UNAUTHORIZED);
    }
}
exports.LoginFailedException = LoginFailedException;
//# sourceMappingURL=login-failed.exception.js.map