import { HttpException } from '@nestjs/common';
export declare class RecoverException extends HttpException {
    constructor(error: string);
}
