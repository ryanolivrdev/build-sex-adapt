"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FavoriteException = void 0;
const common_1 = require("@nestjs/common");
class FavoriteException extends common_1.HttpException {
    constructor(error) {
        super(error, common_1.HttpStatus.CONFLICT);
    }
}
exports.FavoriteException = FavoriteException;
//# sourceMappingURL=favorite.exception.js.map