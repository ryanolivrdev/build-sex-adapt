import { HttpException } from '@nestjs/common';
export declare class TokenInvalidException extends HttpException {
    constructor();
}
