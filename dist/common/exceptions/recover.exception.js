"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecoverException = void 0;
const common_1 = require("@nestjs/common");
class RecoverException extends common_1.HttpException {
    constructor(error) {
        super(error, common_1.HttpStatus.FORBIDDEN);
    }
}
exports.RecoverException = RecoverException;
//# sourceMappingURL=recover.exception.js.map