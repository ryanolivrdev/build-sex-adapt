export declare const HttpCustomMessages: {
    AUTHORIZATION: {
        LOGIN_FAILED: string;
    };
    AUTHENTICATION: {};
    USER: {
        NOT_FOUND: string;
    };
    ESTABLISHMENT: {
        NOT_FOUND: string;
    };
    RECOVER: {
        NOT_FOUND: string;
        IN_PROGRESS: string;
        EXPIRED: string;
        PENDING_CONFIRMATION: string;
        TOKEN: {
            INVALID: string;
            NOT_FOUND: string;
        };
    };
    VALIDATION: {
        WHATSAPP: {
            INVALID: string;
            REQUIRED: string;
        };
        LANDLINE: {
            INVALID: string;
        };
        GROUND_FLOOR_ROOM: {
            INVALID: string;
        };
        ADDRESS: {
            INVALID: string;
            REQUIRED: string;
        };
        WEBSITE: {
            INVALID: string;
            REQUIRED: string;
        };
        CATEGORY: {
            INVALID: string;
            REQUIRED: string;
        };
        PRICE: {
            INVALID: string;
        };
        EMAIL: {
            INVALID: string;
            REQUIRED: string;
        };
        PASSWORD: {
            INVALID: string;
            REQUIRED: string;
            WEAK: string;
        };
        NAME: {
            INVALID: string;
            REQUIRED: string;
        };
        REVIEW: {
            GRADE: {
                INVALID: string;
                REQUIRED: string;
                LENGTH: string;
            };
            COMMENT: {
                INVALID: string;
                REQUIRED: string;
                LENGTH: string;
            };
        };
    };
};
