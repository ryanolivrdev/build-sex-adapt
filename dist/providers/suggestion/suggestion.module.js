"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuggestionModule = void 0;
const user_module_1 = require("../../models/user/user.module");
const common_1 = require("@nestjs/common");
const establishment_module_1 = require("../../models/establishment/establishment.module");
const suggestion_service_1 = require("./suggestion.service");
let SuggestionModule = class SuggestionModule {
};
SuggestionModule = __decorate([
    (0, common_1.Module)({
        imports: [
            (0, common_1.forwardRef)(() => establishment_module_1.EstablishmentModule),
            (0, common_1.forwardRef)(() => user_module_1.UserModule)
        ],
        providers: [suggestion_service_1.SuggestionService],
        exports: [suggestion_service_1.SuggestionService]
    })
], SuggestionModule);
exports.SuggestionModule = SuggestionModule;
//# sourceMappingURL=suggestion.module.js.map