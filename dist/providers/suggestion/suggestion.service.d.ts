import { EstablishmentService } from '@models/establishment/establishment.service';
import { UserService } from '@models/user/user.service';
export declare class SuggestionService {
    private readonly userService;
    private readonly establishmentService;
    constructor(userService: UserService, establishmentService: EstablishmentService);
    generateUserSuggestions(id: string): Promise<{
        stars: number;
        accessibilities: import("../../models/accessibility/entities/accessibility.entity").AccessibilityEntity;
        favoritedBy: import("../../models/user/entities/user.entity").UserEntity[];
        name: string;
        price: number;
        category: string;
        website?: string;
        address: {
            street: string;
            number: string;
            complement: string;
            cep: string;
        };
        ground_floor_room: boolean;
        latitude: number;
        longitude: number;
        cover_photo: string;
        room_photos?: string[];
        landline: string;
        whatsapp?: string;
        reviews: import("../../models/review/entities/review.entity").ReviewEntity[];
        id: string;
        createdAt: Date;
        updatedAt: Date;
    }[]>;
}
