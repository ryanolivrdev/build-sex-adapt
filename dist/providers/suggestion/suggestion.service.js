"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuggestionService = void 0;
const establishment_service_1 = require("../../models/establishment/establishment.service");
const common_1 = require("@nestjs/common");
const user_service_1 = require("../../models/user/user.service");
let SuggestionService = class SuggestionService {
    constructor(userService, establishmentService) {
        this.userService = userService;
        this.establishmentService = establishmentService;
    }
    async generateUserSuggestions(id) {
        const user = await this.userService.findOneOrFail({ where: { id } });
        const establishments = await this.establishmentService.findByAccessibilities(user.accessibilities);
        return establishments;
    }
};
SuggestionService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [user_service_1.UserService,
        establishment_service_1.EstablishmentService])
], SuggestionService);
exports.SuggestionService = SuggestionService;
//# sourceMappingURL=suggestion.service.js.map