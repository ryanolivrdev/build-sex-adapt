import { Repository } from 'typeorm';
import { UserEntity } from '../../models/user/entities/user.entity';
import { EstablishmentEntity } from '../../models/establishment/entities/establishment.entity';
export declare class FavoriteService {
    private readonly userRepository;
    private readonly establishmentRepository;
    constructor(userRepository: Repository<UserEntity>, establishmentRepository: Repository<EstablishmentEntity>);
    getUserFavorites(id: string): Promise<EstablishmentEntity[]>;
    favorite(id: string, establishmentId: string): Promise<UserEntity>;
    unfavorite(id: string, establishmentId: string): Promise<UserEntity>;
}
