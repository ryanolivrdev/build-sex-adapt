"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.FavoriteService = void 0;
const common_1 = require("@nestjs/common");
const favorite_exception_1 = require("../../common/exceptions/favorite.exception");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("../../models/user/entities/user.entity");
const establishment_entity_1 = require("../../models/establishment/entities/establishment.entity");
let FavoriteService = class FavoriteService {
    constructor(userRepository, establishmentRepository) {
        this.userRepository = userRepository;
        this.establishmentRepository = establishmentRepository;
    }
    async getUserFavorites(id) {
        const user = await this.userRepository.findOne({
            where: { id },
            relations: ['favorites']
        });
        return user.favorites;
    }
    async favorite(id, establishmentId) {
        const user = await this.userRepository.findOne({
            where: { id },
            relations: ['favorites']
        });
        const establishment = await this.establishmentRepository.findOne({
            where: {
                id: establishmentId
            }
        });
        if (user.favorites.indexOf(establishment) !== -1)
            throw new favorite_exception_1.FavoriteException('Estabelecimento já foi favoritado.');
        this.userRepository.merge(user, {
            favorites: [...user.favorites, establishment]
        });
        return await this.userRepository.save(user);
    }
    async unfavorite(id, establishmentId) {
        const user = await this.userRepository.findOne({
            where: { id },
            relations: ['favorites']
        });
        const establishment = await this.establishmentRepository.findOne({
            where: {
                id: establishmentId
            }
        });
        user.favorites.splice(user.favorites.indexOf(establishment), 1);
        await this.establishmentRepository.save(establishment);
        return await this.userRepository.save(user);
    }
};
FavoriteService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(user_entity_1.UserEntity)),
    __param(1, (0, typeorm_1.InjectRepository)(establishment_entity_1.EstablishmentEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], FavoriteService);
exports.FavoriteService = FavoriteService;
//# sourceMappingURL=favorite.service.js.map