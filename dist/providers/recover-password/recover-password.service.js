"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecoverPasswordService = void 0;
const mail_service_1 = require("./../../services/mail/mail.service");
const token_invalid_exception_1 = require("./../../common/exceptions/token-invalid.exception");
const index_messages_1 = require("../../common/helpers/exceptions/messages/index.messages");
const user_service_1 = require("./../../models/user/user.service");
const recover_password_entity_1 = require("./../../models/recover-password/entities/recover-password.entity");
const typeorm_1 = require("typeorm");
const common_1 = require("@nestjs/common");
const typeorm_2 = require("@nestjs/typeorm");
const exceptions_1 = require("../../common/exceptions");
const recover_exception_1 = require("../../common/exceptions/recover.exception");
let RecoverPasswordService = class RecoverPasswordService {
    constructor(recoverRepository, userService, mailService) {
        this.recoverRepository = recoverRepository;
        this.userService = userService;
        this.mailService = mailService;
    }
    async create(createRecoverPasswordDto) {
        try {
            const existsRecover = await this.recoverRepository.findOne({
                where: { email: createRecoverPasswordDto.email }
            });
            if (existsRecover)
                throw new recover_exception_1.RecoverException(index_messages_1.HttpCustomMessages.RECOVER.IN_PROGRESS);
            const recover = this.recoverRepository.create({
                email: createRecoverPasswordDto.email
            });
            const saved_recover = await this.recoverRepository.save(recover);
            if (!saved_recover)
                throw new recover_exception_1.RecoverException('Erro inesperado ao salvar sua recuperação entre em contato com suporte.');
            await this.mailService.mailRecoverToken(createRecoverPasswordDto.email, recover.token);
            return;
        }
        catch (err) {
            return err;
        }
    }
    async confirmToken(confirmTokenDto) {
        const recover = await this.recoverRepository.findOne({
            where: {
                email: confirmTokenDto.email
            }
        });
        if (recover.createdAt.getMinutes() > recover.createdAt.getMinutes() + 10) {
            await this.create({ email: confirmTokenDto.email });
            throw new recover_exception_1.RecoverException(index_messages_1.HttpCustomMessages.RECOVER.EXPIRED);
        }
        if (!recover)
            throw new recover_exception_1.RecoverException(index_messages_1.HttpCustomMessages.RECOVER.NOT_FOUND);
        if (!(recover.token === confirmTokenDto.token))
            return new token_invalid_exception_1.TokenInvalidException();
        return await this.recoverRepository.update({ email: confirmTokenDto.email }, {
            status: 'CHANGING'
        });
    }
    async changePassword(changePasswordDto) {
        const recover = await this.recoverRepository.findOne({
            where: {
                email: changePasswordDto.email
            }
        });
        if (!recover)
            throw new recover_exception_1.RecoverException(index_messages_1.HttpCustomMessages.RECOVER.NOT_FOUND);
        if (recover.status !== 'CHANGING')
            throw new recover_exception_1.RecoverException(index_messages_1.HttpCustomMessages.RECOVER.PENDING_CONFIRMATION);
        const user = await this.userService.findOneOrFail({
            where: { email: changePasswordDto.email }
        });
        if (!user)
            throw new exceptions_1.NotFoundException(index_messages_1.HttpCustomMessages.USER.NOT_FOUND);
        await this.recoverRepository.delete({ email: changePasswordDto.email });
        const updatedUser = await this.userService.update(user.id, {
            password: changePasswordDto.password
        });
        delete updatedUser.createdAt;
        delete updatedUser.updatedAt;
        delete updatedUser.password;
        delete updatedUser.hashedRefreshToken;
        return updatedUser;
    }
};
RecoverPasswordService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_2.InjectRepository)(recover_password_entity_1.RecoverPasswordEntity)),
    __metadata("design:paramtypes", [typeorm_1.Repository,
        user_service_1.UserService,
        mail_service_1.MailService])
], RecoverPasswordService);
exports.RecoverPasswordService = RecoverPasswordService;
//# sourceMappingURL=recover-password.service.js.map