"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RecoverPasswordModule = void 0;
const mail_module_1 = require("./../../services/mail/mail.module");
const user_service_1 = require("../../models/user/user.service");
const user_entity_1 = require("../../models/user/entities/user.entity");
const user_module_1 = require("../../models/user/user.module");
const recover_password_entity_1 = require("./../../models/recover-password/entities/recover-password.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const recover_password_service_1 = require("./recover-password.service");
const accessibility_entity_1 = require("../../models/accessibility/entities/accessibility.entity");
let RecoverPasswordModule = class RecoverPasswordModule {
};
RecoverPasswordModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                recover_password_entity_1.RecoverPasswordEntity,
                user_entity_1.UserEntity,
                accessibility_entity_1.AccessibilityEntity
            ]),
            user_module_1.UserModule,
            mail_module_1.MailModule
        ],
        controllers: [],
        providers: [recover_password_service_1.RecoverPasswordService, user_service_1.UserService],
        exports: [recover_password_service_1.RecoverPasswordService]
    })
], RecoverPasswordModule);
exports.RecoverPasswordModule = RecoverPasswordModule;
//# sourceMappingURL=recover-password.module.js.map