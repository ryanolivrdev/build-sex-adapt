import { MailService } from './../../services/mail/mail.service';
import { TokenInvalidException } from './../../common/exceptions/token-invalid.exception';
import { CreateRecoverPasswordDto } from './../../models/recover-password/dto/create-recover-password.dto';
import { UserService } from './../../models/user/user.service';
import { RecoverPasswordEntity } from './../../models/recover-password/entities/recover-password.entity';
import { Repository } from 'typeorm';
import { ConfirmTokenDto } from '@models/recover-password/dto/confirm-token.dto';
import { ChangePasswordDto } from '@models/recover-password/dto/change-password.dto';
export declare class RecoverPasswordService {
    private readonly recoverRepository;
    private userService;
    private mailService;
    constructor(recoverRepository: Repository<RecoverPasswordEntity>, userService: UserService, mailService: MailService);
    create(createRecoverPasswordDto: CreateRecoverPasswordDto): Promise<any>;
    confirmToken(confirmTokenDto: ConfirmTokenDto): Promise<TokenInvalidException | import("typeorm").UpdateResult>;
    changePassword(changePasswordDto: ChangePasswordDto): Promise<import("../../models/user/entities/user.entity").UserEntity>;
}
